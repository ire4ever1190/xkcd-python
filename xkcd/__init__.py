import aiohttp
from random import randint

class getter():
	def __init__(self):
		# Just a high number unless someone calls the next function to get the right number
		self.total_num = 2000

	async def get_num_comics(self):
		async with aiohttp.ClientSession() as session:
			async with session.get(url='https://xkcd.com/info.0.json') as resp:
				json = await resp.json()
				self.total_num = int(json['num'])

	async def random(self):
		comic_num = randint(1, self.total_num)
		async with aiohttp.ClientSession() as session:
			async with session.get(f'https://xkcd.com/{comic_num}/info.0.json') as resp:
				json = await resp.json()
				return json['img']

