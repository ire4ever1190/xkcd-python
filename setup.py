from setuptools import setup

setup(
	name='xkcd',
	version='1.1',
	packages=['xkcd'],
	url='',
	license='',
	author='Jake Leahy',
	install_requires=[
		'aiohttp',
		'cchardet',
		'aiodns'
	],
	author_email='',
	description='Small package that uses aiohttp to fetch a random xkcd comic'
)
